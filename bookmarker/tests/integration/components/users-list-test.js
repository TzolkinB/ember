import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

module('Integration | Component | users-list', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`{{users-list fake-url sample-email}}`);

    assert.equal(this.element.querySelector('img').getAttribute('class'), 'avatar');

    // Template block usage: 
    await render(hbs`
      {{#users-list fake-url sample-email}}
        template block text
      {{/users-list}}
    `);

    assert.equal(this.element.querySelector('img').getAttribute('class'), 'avatar');
  });
});
