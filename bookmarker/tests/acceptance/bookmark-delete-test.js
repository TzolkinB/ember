import { module, test } from 'qunit';
import { setupApplicationTest } from 'ember-qunit';

import { visit, currentURL, click } from '@ember/test-helpers';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';


module('Acceptance | bookmark delete', function(hooks) {
  setupApplicationTest(hooks);
  setupMirage(hooks);

  test('visiting /bookmark-delete', async function(assert) {
    // await visit('/bookmark-delete');

    // assert.equal(currentURL(), '/bookmark-delete');
    let user = server.create('user', {});
    let bookmark = server.create('bookmark', {
    	title: 'Test Bookmark',
    	userId: user.id,
  	});
  	await visit(`/bookmarks/edit/${bookmark.id}`);
  	await click('button#delete');
  	assert.equal(currentURL(), '/bookmarks');
  });
});
