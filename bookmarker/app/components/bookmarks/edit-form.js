import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
	bookmark: null,
	//router: inject.service('-routing'),
	router: service(),

	actions: {
		save(bookmark) {console.log('save called');
		bookmark.save()
		.then(
			(value) => {
				console.log(value);
				this.router.transitionTo('bookmarks');
			}
		)
		.catch(
			() => {
				console.log('failed to save');
			}
		)
	},
		cancel() {console.log('cancel called');},
		delete(bookmark) {
			bookmark.destroyRecord();
			this.router.transitionTo('bookmarks');
		}
	}
});
