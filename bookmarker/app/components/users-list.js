import Component from '@ember/component';

const UsersListComponent = Component.extend({
	imgClass: "avatar",
	click() {
		//Ember.Logger.info("users-list was clicked");
		//Ember.Logger is deprecated, use console.log instead
		console.log('users-list was clicked');
		return false;  
	}
});

 UsersListComponent.reopenClass({
 	positionalParams: ['avatarUrl', 'email']
 });
 export default UsersListComponent;