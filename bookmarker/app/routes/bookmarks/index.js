import Route from '@ember/routing/route';

export default Route.extend({
	model() {
		//return this.get('store').findAll('bookmark');
		return this.store.findAll('bookmark');
	},
	renderTemplate() {
		this.render('about');
	}
});