export default function(server) {

  /*
    Seed your development database using your factories.
    This data will not be loaded in your tests.
  */

  // server.createList('post', 10);

  // name of model and number of instances to create
  server.createList('user', 10); // creates 10 users
  server.createList('bookmark', 20); //creates 20 bookmarks 
}
